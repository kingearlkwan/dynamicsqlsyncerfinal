#coding : utf-8
#This program serves as media for transferring data from ProdData table of MS Access database to SQL database table.
#It creates and updates all records from MS Access to SQL.
#All created and updated records can be seen on the logfile


require 'win32ole'
require 'rubygems'
require 'active_record'
require 'activerecord-sqlserver-adapter'
require 'tiny_tds'
require 'log4r'
require 'win32API'
require 'net/smtp'
require 'mail'

#Log info and error during the program run
Log = Log4r::Logger.new("LogProdData")
Log.outputters << Log4r::Outputter.stderr
Dir.mkdir('log') unless Dir.exist?('log')
file = Log4r::FileOutputter.new('LogProdData',:filename=>'log/LogProdData.log')
file.formatter = Log4r::PatternFormatter.new(:pattern=>"[%l] %d :: %m")
Log.outputters << file



SW_HIDE = 0
WS_OVERLAPPED = 0
WS_DISABLED = 0x08000000
WS_MINIMIZED = 0x20000000

def already_running
	
	findwindow = Win32API.new('user32','FindWindow', ['p','p'], 'L')
	return true if findwindow.call("STATIC", "Rhi4FileProcessorHiddenWindowProd") != 0
	createwindow = Win32API.new('user32', 'CreateWindowEx', ['l', 'p','p', 'l', 'i', 'i', 'i', 'i', 
		'l', 'l', 'l', 'P'], 'l')
	dwStyle = (WS_DISABLED | WS_MINIMIZED)
	cw = createwindow.call( WS_OVERLAPPED, 'STATIC','Rhi4FileProcessorHiddenWindowProd', dwStyle, 0, 0, 0, 0, 0, 0, 0, 0 )
	show = Win32API.new('user32',  'ShowWindow', ['L', 'L'], 'L')
	show.call(cw, SW_HIDE)
	return false
end



if already_running
	Log.info "Sorry Application is already running!"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



#YAML Configuration
begin

	@config=YAML.load_file Dir.pwd+'/config.yml'	
	
	
rescue Exception => e
	Log.error "Unable to find 'config.yml'.#{e}"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	exit
end



# Email Notification set up for error
$recipient = @config['to']
$sender = @config['from']
$postemail = @config['postmaster']
$postpassword = @config['postpassword']


smtp = Net::SMTP.new 'smtp.gmail.com', 587
smtp.enable_starttls
smtp.start($postemail,$postemail,$postpassword,:login) do



#Set up MSaccess connection
$table_n=@config['tablename']
$primary_k=@config['primarykey']

class AccessDb
    attr_accessor :mdb, :connection, :data, :fields

    def initialize(mdb=nil)
        @mdb = mdb
        @connection = nil
        @data = nil
        @fields = nil
    end

    def open 
        connection_string =  'Provider=Microsoft.Jet.OLEDB.4.0;Data Source='
        connection_string << @mdb
        @connection = WIN32OLE.new('ADODB.Connection')
        @connection.Open(connection_string)
    end

    def query(sql)
        recordset = WIN32OLE.new('ADODB.Recordset')
        recordset.Open(sql, @connection)
        @fields = []
        recordset.Fields.each do |field|
            @fields << field.Name
        end
        begin
            @data = recordset.GetRows.transpose
        rescue
            @data = []
        end
        recordset.Close
    end

    def execute(sql)
        @connection.Execute(sql)
    end

    def close
        @connection.Close
    end
end



begin

	db = AccessDb.new(@config['access_path'])
	db.open
		
		#Check number of records in MS ACCESS,Reference for record creation in sql
		db.query("SELECT * FROM #{@config['accesstable']} WHERE a_created_at is null ORDER BY Id")
			$field_names_all = db.fields
			rows_all = db.data
			
		#update query
		db.query("SELECT * FROM #{@config['accesstable']} WHERE Datetime_Updated is not null ORDER BY Id")
			field_complete = db.fields
			rows_complete = db.data
			
		#datetime_updated query
		db.query("SELECT Datetime_Updated FROM #{@config['accesstable']} WHERE Datetime_Updated is not null ORDER BY Id")
			accessDatetime_Updated = db.fields
			accessrows_Datetime_Updated = db.data
			
		# #proddata update query
		# db.query("SELECT * FROM #{@config['accesstable']} WHERE Status<>'5' or Status is null ORDER BY Id")
			# field_update = db.fields
			# rows_update = db.data
			
		# #proddata complete query
		# db.query("SELECT * FROM #{@config['accesstable']} WHERE Status='5' and CompleteSync is null ORDER BY Id")
			# field_complete = db.fields
			# rows_complete = db.data

			
	rescue Exception => e
		Log.error "Could Not Connect to MS Access Database.#{e}"
		Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
		$msg = "Subject: ProdData SQL Syncer Error \n\n\n #{e}.\n Cannot connect to MS Access Database. \n\n Date & Time: #{Time.now}"
		smtp.send_message($msg, $sender, $recipient)
		exit
end


#Connect to SQL Server		
ActiveRecord::Base.default_timezone = :local
ActiveRecord::Base.establish_connection(
	
	:adapter=>"sqlserver",
	:mode=>'dblib',
	:host=>@config['host'],
	:port=>@config['port'],
	:database=>@config['database'],
	:username=>@config['username'],
	:password=>@config['password'],
	:timeout=>@config['timeout'],
	:azure=>'false'

)

# Define Active records for SQL	
class Boo < ActiveRecord::Base

	self.table_name=$table_n
	self.primary_key=$primary_k

	
end



begin
Boo.connection
rescue Exception => e
	Log.error "Could Not Connect to SQL Server.#{e}"
	Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
	$msg = "Subject: ProdData Syncer Error \n\n\n #{e}. Could not connect to SQL Server \n\n Date & Time: #{Time.now}"
	smtp.send_message($msg, $sender, $recipient)
	exit
end



def SpecialCase(accessrecord,sqlrecord,i)

begin
	$field_names_all.each do|fieldaccess|
	
	fieldaccess=fieldaccess.force_encoding("#{fieldaccess.encoding}").encode('UTF-8')
	fieldaccess=fieldaccess[0].upcase + fieldaccess[1..-1]
	
	
	case fieldaccess
	
	when 'Id'
		fieldaccess='SQL_ID'
		sqlrecord.send("#{fieldaccess}=",accessrecord[i])
		i=i+1
		
	when 'CompleteSync'
		break
	
	when 'Datetime_Updated'
		break
		
	else
		
		if accessrecord[i].is_a? String #and !accessrecord[i].eql? nil
		
			converteddata=accessrecord[i].force_encoding("#{accessrecord[i].encoding}").encode('UTF-8')
			sqlrecord.send("#{fieldaccess}=",converteddata)
			i=i+1
		
		else
		sqlrecord.send("#{fieldaccess}=",accessrecord[i])
		i=i+1
		end
	
	end
	
	end

	rescue Exception => e
					Log.error "Error updating SQL record.#{e}"
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					exit
	
end
	
end 
def DateCreated(accessrecord,db,id_access)

	recentCreate = Boo.where("SQL_ID=#{id_access}").take
	create=recentCreate.created_at
	accessid=id_access
	strdate=create.strftime("%m,%d,%y,%H,%M,%S").split(",")
	newdate="#{strdate[0]}/#{strdate[1]}/#{strdate[2]} #{strdate[3]}:#{strdate[4]}:#{strdate[5]}"
	db.execute("UPDATE #{@config['accesstable']} SET a_created_at='#{newdate}' WHERE id=#{accessid};")
	


end


def DateUpdated(accessrecord,db,id_access)

		recentUpdate = Boo.where("SQL_ID=#{id_access}").take
		updatedatcur=recentUpdate.updated_at
		accessid=id_access
		strdateupdate=updatedatcur.strftime("%m,%d,%y,%H,%M,%S").split(",")
		newdateupdate="#{strdateupdate[0]}/#{strdateupdate[1]}/#{strdateupdate[2]} #{strdateupdate[3]}:#{strdateupdate[4]}:#{strdateupdate[5]}"
		db.execute("UPDATE #{@config['accesstable']} SET a_updated_at='#{newdateupdate}' WHERE id=#{accessid};")

end


def DateFormatter(givendate)

	gendatetime=givendate.strftime("%Y/%m/%d/%H/%M/%S").split("/")#datetime month day yr as array
	dtgen = DateTime.new(gendatetime[0].to_i, gendatetime[1].to_i, gendatetime[2].to_i,gendatetime[3].to_i,gendatetime[4].to_i,gendatetime[5].to_i).change(:offset => "+0800")
	
	return dtgen

end


begin

#p rows_all.length
for i in (0)..(rows_all.length-1)

	x=rows_all[i]
	j=0
	$field_names_all.each do|fieldaccess|
		 case fieldaccess
	
			when 'id'
				$accessid=x[j]
				break
			
			else
			p j=j+1
	
		 end
	 end
	
	sqlCheck=Boo.where("SQL_ID=#{$accessid}").take

	if sqlCheck.eql? nil
	
		sqlcreate = Boo.new
		SpecialCase(x,sqlcreate,0)
		sqlcreate.Subsidiary=@config['subsidiary']
		sqlcreate.Crop_Year=@config['cropyear']
		sqlcreate.save
		DateCreated(x,db,$accessid)
		Log.info "creating SQL_ID #{$accessid}"
	
	else

		DateCreated(x,db,$accessid)
		Log.info "updating MS access Id #{$accessid}"
	
	end
	
end
  
for i in 0..(rows_complete.length-1)

	accessupdate=rows_complete[i]
	j=0
	 $field_names_all.each do|fieldaccess|
	
		 case fieldaccess
	
		 when 'id'
			 $accessid=accessupdate[j]
			 break
		 else
		 j=j+1
	
		 end
	 end
	
	sqldat=Boo.where("SQL_ID=#{$accessid}").take
	accessDateTimeUpdated=accessrows_Datetime_Updated[i]

	access_update=DateFormatter(accessDateTimeUpdated[0])
	
	if access_update> sqldat.updated_at
		SpecialCase(accessupdate,sqldat,0)
		sqldat.save
		DateUpdated(accessupdate,db,$accessid)
	
		Log.info "updating SQL_ID #{$accessid}"
	end
end


rescue Exception => e
					Log.error "#{e}"
					Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"
					exit

end
  
db.close

end

Log.info "\n-----------------------------------------------------END-------------------------------------------------------------"





